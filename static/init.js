require.config({
	baseUrl: "/static/",
	paths: {
		"backbone": "backbone/backbone",
		"jquery": "jquery/dist/jquery",
		"underscore": "underscore/underscore",
		"marionette": "marionette/lib/backbone.marionette",
		"bootstrap": "bootstrap/dist/js/bootstrap",
		"bootstrap-table": "bootstrap-table/dist/bootstrap-table",
        "bootstrap-modal": "bootstrap/js/modal",
		"text": "text/text"
	},
	shim: {
		"bootstrap": {"deps": ["jquery"]},
		"marionette": {"deps": ["jquery", "underscore", "backbone"]},
		"backbone": {"deps": ["underscore"]},
        "bootstrap-table": {"deps": ["jquery"]},
        "bootstrap-modal": {"deps": ["bootstrap"]}
	}

})

require(["jquery", "app", "js/ClientTableView", "js/VoteView"], function($, app, ClientTableView, VoteView){
    var clientView = new ClientTableView();
    var voteView = new VoteView();
    app.getRegion("clients").show(clientView);
    app.getRegion("voting").show(voteView);

    $("#client-table").click(function(){
    	clientView.update();
    });

    $("#voting-view").click(function(){
    	voteView.update();
    });

});