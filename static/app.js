define(["marionette"], function(Mn){
    var Application = Mn.Application.extend({
         regions: {
             clients: "#clients",
             voting: "#voting"
         }
    }); 
    
    var app = new Application();
    app.on("start", function(){
        Backbone.history.start();
    });
    return app;
});