define(["text!tableView.html", "jquery","backbone", "marionette", "bootstrap", "bootstrap-table"], function(html, $, Backbone, Mn){
	var template = $(html);
    var FILE_LINK = "/export/";
    var csrfToken = null;
    var cookies = document.cookie.split(";");
    for (var i in cookies){
        if (cookies[i].indexOf("csrftoken") >= 0){
            csrfToken = cookies[i].split("=")[1];
        }
    }

	var ControlView = Mn.LayoutView.extend({
		template: template.filter("#table-controls")[0].outerHTML,
        className: "toolbar",
        ui: {
            export: "#export",
            remove: "#remove",
            add: "#add",
            save: "#save-client",
            cancel: "#cancel-client"      
        },
        events: {
            "click @ui.export": "export",
            "click @ui.remove": "remove",
            "click @ui.add": "add",
            "click @ui.save": "save",
            "click @ui.cancel": "clearError"
        },
        
        initialize: function(table){
            this.table = table;
        },
        
        export: function(e){
            window.open("/clients/export/");
        },
        
        remove: function(e){
            var deleteObjectsIDs = this.table.bootstrapTable("getAllSelections").map(function(item){
                return item.id;
            });
            var data = new FormData();
            data.append("ids", deleteObjectsIDs);
            console.debug(data);
            $.ajax({
                method: "DELETE",
                url: "/clients/clients/",
                data: JSON.stringify({ids: deleteObjectsIDs}),
                dataType: "json",
                contentType: "application/json",
                context: this,
                beforeSend: function(xhr){
                    xhr.setRequestHeader("X-CSRFToken", csrfToken);
                },
                success: function(){
                    this.table.bootstrapTable("refresh");
                }
            });
        },
        
        add: function(e){
            this.showModal();
        },
        
        showModal: function(){
            this.$el.find(".modal").modal("show");
        },
        
        hideModal: function(){
            this.$el.find(".modal").modal("hide");  
        },
        
        clearError: function(){
            this.$el.find(".text-danger").remove();
        },
        
        addError: function(errors){
            this.$el.find("input").each(function(index, element){
                var fieldErrors = errors[element.id];
                if (fieldErrors){
                    $(element).after("<p class='text-danger'>" + fieldErrors[0] + "</p>");   
                }
            });
        },  
        
        save: function(e){
            var user_data = new FormData();
            this.$el.find(".modal-body input").each(function(i, item){
                if (item.type == "file"){
                    user_data.append(item.id, item.files[0]);
                }
                else{
                    user_data.append(item.id, item.value);    
                }
            });
            $.ajax({
                method: "POST",
                url: "/clients/clients/",
                data: user_data,
                dataType: "json",
                context: this,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function(xhr){
                    xhr.setRequestHeader("X-CSRFToken", csrfToken);
                },
                success: function(){
                    this.clearError();
                    this.hideModal();
                    this.table.bootstrapTable("refresh");
                },
                error: function(response){
                    this.clearError();
                    this.addError(response.responseJSON);
                }
            })
        }
	});

	var TableView = Mn.LayoutView.extend({
        template:  template.filter("#clientTable")[0].outerHTML,
        initialize: function(){
          this.table = null;  
        },
        
        initTable: function(el){
            el.bootstrapTable({
                responseHandler: this.responseHandler,
                queryParams: this.formatQueryParams,
                columns: [
                        {
                            field: "state",
                            checkbox: true,
                            align: "center",
                            valign: "middle"
                        }, {
                            title: "Name",
                            field: "name",
                            align: "center",
                            valign: "middle",
                            sortable: true
                        }, {
                            title: "Surname",
                            field: "surname",
                            align: "center",
                            sortable: true
                        },
                        {
                            title: "Birthdate",
                            field: "birthdate",
                            align: "center",
                            sortable: true
                        },
                        {
                            title: "Age",
                            field: "age",
                            align: "center",
                            sortable: true
                        }]
            });
        },
        
        onRender: function(){
          this.table = this.$el.find("#contentTable");
          this.initTable(this.table);
        },
        
        responseHandler: function(response){
            return {rows: response.results,
                    total: response.results.Length};
        },
        
        formatQueryParams: function (params){
            if (params["sort"]){
                var orderField = params["sort"];
                delete params["sort"];
                params["ordering"] = (params["order"] == "desc"? "-": "") + orderField
            }
            return params;
        }        
	});

	var ClientTableView = Mn.LayoutView.extend({
		template: template.filter("#table")[0].outerHTML,
		regions: {
			"controls": ".controls",
			"table": ".table"
		},

		onRender: function(){
            this.tableView = new TableView();
            this.getRegion("table").show(this.tableView);
            this.controlView = new ControlView(this.tableView.table);
            this.getRegion("controls").show(this.controlView);
		},

        update: function(){
            this.tableView.table.bootstrapTable("refresh");
        }
	});
	return ClientTableView;
});