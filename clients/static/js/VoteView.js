define(["text!voteView.html", "backbone", "marionette"], function(html, Backbone, Mn){
	var template = $(html);
	var VoteCollection = Backbone.Collection.extend({
		url: "/clients/votes/",
		parse: function(response){
			return response.results;
		}
	});

	var csrfToken = null;
    var cookies = document.cookie.split(";");
    for (var i in cookies){
        if (cookies[i].indexOf("csrftoken") >= 0){
            csrfToken = cookies[i].split("=")[1];
        }
    }

	var ChoiceView = Mn.ItemView.extend({
		template: template.filter("#choice")[0].outerHTML,
        className: "item-vote-view",
    	events: {
            "click #up": "up",
            "click #down": "down"
        },
        up: function(){
        	this.vote(1);
        },

        down: function(){
        	this.vote(-1);
        },

        vote: function(value){
        	$.ajax({
                method: "POST",
                url: "/clients/elect/",
                data: JSON.stringify({value: value, id: this.model.get("id")}),
                context: this,
                dataType: "json",
                beforeSend: function(xhr){
                    xhr.setRequestHeader("X-CSRFToken", csrfToken);
                },
                success: function(response){
                    this.model.set("value", response.value);
                    this.render();
                }
            })
        }
	});

	var VoteView = Mn.CollectionView.extend({
		childView: ChoiceView,
        className: "vote-view",
		initialize: function(){
			this.collection = new VoteCollection();
			this.collection.fetch();
		},

        update: function(){
            this.collection.fetch();
        }
	});



	return VoteView;
});