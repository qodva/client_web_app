# -*- coding:utf-8 -*-
from __future__ import unicode_literals

import json
try:
    from io import StringIO
except:
    from StringIO import StringIO

from django.db.models import F
from django.http import JsonResponse, HttpResponse
from rest_framework import viewsets
from rest_framework import filters
from rest_framework.parsers import JSONParser, FileUploadParser, MultiPartParser

from main.celery import export_xlsx
from .models import Client, Vote
from .serializers import ClientSerializer, VoteSerializer


class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
    filter_backends = (filters.DjangoFilterBackend,
                       filters.OrderingFilter,
                       filters.SearchFilter)
    filter_fields = ("name", "surname")
    order_fields = "__all__"
    search_fields = ("name", "surname")
    parsers = (FileUploadParser, MultiPartParser, JSONParser)

    def delete(self, request, pk=None):
        remove_ids = request.data.get("ids", None)
        Client.objects.filter(pk__in=remove_ids).delete()
        return JsonResponse({"status": "ok"})


class VoteViewSet(viewsets.ModelViewSet):
    queryset = Vote.objects.all()
    serializer_class = VoteSerializer


def elect(request):
    request_params = json.loads(request.body.decode("utf-8"))
    vote_id, value_delta = [request_params[i] for i in ["id", "value"]]
    vote_value = change_vote(vote_id, value_delta)
    return JsonResponse({"value": vote_value})


def change_vote(vote_id, change_value=1):
    from django.db import transaction
    with transaction.atomic():
        vote_item = Vote.objects.select_for_update().get(id=vote_id)
        if (vote_item.value == 10 and change_value > 0) or \
           (vote_item.value == 0 and change_value < 0):
           raise Exception()
        vote_item.value = F("value") + change_value
        vote_item.save()
        vote_item.refresh_from_db()
    return vote_item.value


def export(request):
    response = HttpResponse(export_xlsx.delay().get(),
                            content_type="application/vnd.ms-excel")
    response["Content-Disposition"] = "attachment; filename=export.xlsx"
    return response
