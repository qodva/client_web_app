# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework import routers

from .views import ClientViewSet, VoteViewSet

client_router = routers.DefaultRouter()
client_router.register(r"clients", ClientViewSet)
client_router.register(r"votes", VoteViewSet)
