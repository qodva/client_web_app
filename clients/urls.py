# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url, include

from .routers import client_router
from .views import export, elect

urlpatterns = [
    url(r"", include(client_router.urls)),
    url(r"export", export),
    url(r"elect", elect)
]
