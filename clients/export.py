# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from openpyxl import Workbook
from openpyxl.writer.excel import save_virtual_workbook

class XlsxWriter(object):
    def __init__(self, out_stream=None):
        self.out_stream = out_stream
        self.__wb = Workbook()
        self.__ws = self.__wb.active

    def write_header(self, headers):
        self.__ws.append(headers)

    def write(self, row):
        self.__ws.append(row)

    def close(self):
        self.__wb.save(self.out_stream)

    def virtual_save(self):
        return save_virtual_workbook(self.__wb)
