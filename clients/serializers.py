# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework import serializers
from .models import Client, Vote

class ClientSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Client
        fields = ("id", "name", "surname", "birthdate", "age", "photo")

class VoteSerializer(serializers.ModelSerializer):

    client_photo = serializers.ImageField(source="client.photo", read_only=True)

    class Meta:
        model = Vote
        fields = ("id", "client_photo", "value")
