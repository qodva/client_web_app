# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

class Client(models.Model):

    name = models.CharField(
        max_length=32,
        verbose_name="Имя")

    surname = models.CharField(
        max_length=64,
        verbose_name="Фамилия")

    age = models.IntegerField(
        validators=[MinValueValidator(0), MaxValueValidator(128)],
        verbose_name="Возраст")

    birthdate = models.DateField(
        verbose_name="Дата рождения"
    )

    photo = models.ImageField(
        verbose_name="Фото")

    class Meta:
        ordering = ("surname", )

    def save(self, *args, **kwargs):
        super(Client, self).save(*args, **kwargs)
        Vote.objects.update_or_create(client=self)


class Vote(models.Model):

    client = models.ForeignKey(Client,
        verbose_name="Клиент")

    value = models.IntegerField(
        default=0)
