# -*- coding:utf-8 -*-
from __future__ import unicode_literals, absolute_import

from clients.export import XlsxWriter
import os
from celery import Celery

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "main.settings")

from django.conf import settings
app = Celery("main")
app.config_from_object("django.conf:settings")
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)


@app.task
def export_xlsx():
    from clients.models import Client
    writer = XlsxWriter()
    for item in Client.objects.all():
        row_data = []
        row_data.append(item.name)
        row_data.append(item.surname)
        row_data.append(item.birthdate)
        row_data.append(item.age)
        writer.write(row_data)
    return writer.virtual_save()
